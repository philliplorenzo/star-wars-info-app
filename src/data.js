export default {
  pictures: [
    {
      'id': 0,
      'url': 'http://a.dilcdn.com/bl/wp-content/uploads/sites/6/2017/01/sw-the-last-jedi-tall-B.jpg',
      'comment': 'Trailer Plays Tonight During MNF Halftime',
      'info': 'Info from ESPN'
    },
    {
      'id': 1,
      'url': 'https://lumiere-a.akamaihd.net/v1/images/tlj-trailer-thumb-swcom_dca48701.jpeg?region=0%2C0%2C1920%2C1080',
      'comment': 'Star Wars The Last Jedi Tickets Go On Sale Today',
      'info': 'Info from AP'
    },
    {
      'id': 2,
      'url': 'https://cdn.vox-cdn.com/thumbor/5k5TZfMDCFs8NPBQecdnZYE00T4=/0x0:810x1200/1200x800/filters:focal(339x190:467x318)/cdn.vox-cdn.com/uploads/chorus_image/image/57075449/DLvAUShUIAASVox.1507596260.jpg',
      'comment': 'SW: The Last Jedi Poster Reveal',
      'info': 'Courtesy of the Star Wars Show'
    },
    {
      'id': 3,
      'url': 'https://media.vanityfair.com/photos/592457a5dffe772993b30b3f/master/w_1440,c_limit/star-wars-portfolio-06-2017-ss14.jpg',
      'comment': 'New Details on the Villains of The Last Jedi',
      'info': 'Info from Vanity Fair'
    },
    {
      'id': 4,
      'url': 'http://a.dilcdn.com/bl/wp-content/uploads/sites/6/2017/07/star-wars-cover-17.jpg',
      'comment': 'Understanding The Impact Of Princess Leia on the Star Wars Universe',
      'info': 'Article By Alissa Renz'
    }
  ]
}

